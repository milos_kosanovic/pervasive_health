#include <tizen.h>
#include "nativewidget.h"
#include "sensor.h"

typedef struct widget_instance_data {
	Evas_Object *win;
	Evas_Object *conform;
	Evas_Object *label;
	Evas_Object *label0; /* Whether the accelerator sensor is supported */
	Evas_Object *label1; /* Current acceleration value */
	Evas_Object *label2; /* Maximum acceleration value */
} widget_instance_data_s;

typedef struct _sensor_info {
    sensor_h sensor; /* Sensor handle */
    sensor_listener_h sensor_listener; /* Sensor listener */
} sensorinfo_s;

static sensorinfo_s sensor_info;
static float max_acc_value[3] = {0.f, 0.f, 0.f};

static float
get_absolute_max(float value1, float value2)
{
    float v1 = value1 > 0.f ? value1 : -value1;
    float v2 = value2 > 0.f ? value2 : -value2;
    float result = v1 > v2 ? v1 : v2;

    return result;
}

static void
show_is_supported( widget_instance_data_s *ad)
{
    char buf[PATH_MAX];
    bool is_supported = false;
    sensor_is_supported(SENSOR_ACCELEROMETER, &is_supported);
    sprintf(buf, "Acc  %s", is_supported ? "yes" : "no");
    elm_object_text_set(ad->label0, buf);
}

static void
_new_sensor_value(sensor_h sensor, sensor_event_s *sensor_data, void *user_data)
{
    if (sensor_data->value_count < 3)
                return;
            char buf[PATH_MAX];
            widget_instance_data_s *ad = (widget_instance_data_s*)user_data;

            sprintf(buf, "V: -X : %0.1f Y : %0.1f Z : %0.1f",
                    sensor_data->values[0], sensor_data->values[1], sensor_data->values[2]);
            elm_object_text_set(ad->label1, buf);

            for (int i = 0; i < 3; i++)
                max_acc_value[i] = get_absolute_max(max_acc_value[i], sensor_data->values[i]);

            sprintf(buf, "Max: X: %0.1f Y: %0.1f Z: %0.1f",
                    max_acc_value[0], max_acc_value[1], max_acc_value[2]);
            elm_object_text_set(ad->label2, buf);
}

static void
start_accelerator_sensor(widget_instance_data_s *ad)
{
    sensor_error_e err = SENSOR_ERROR_NONE;
    sensor_get_default_sensor(SENSOR_ACCELEROMETER, &sensor_info.sensor);
    err = sensor_create_listener(sensor_info.sensor, &sensor_info.sensor_listener);
    sensor_listener_set_event_cb(sensor_info.sensor_listener, 100, _new_sensor_value, ad);
    sensor_listener_start(sensor_info.sensor_listener);
}



static int
widget_instance_create(widget_context_h context, bundle *content, int w, int h, void *user_data)
{
	widget_instance_data_s *wid = (widget_instance_data_s*) malloc(sizeof(widget_instance_data_s));
	int ret;

	if (content != NULL) {
		/* Recover the previous status with the bundle object. */

	}

	/* Window */
	ret = widget_app_get_elm_win(context, &wid->win);
	if (ret != WIDGET_ERROR_NONE) {
		dlog_print(DLOG_ERROR, LOG_TAG, "failed to get window. err = %d", ret);
		return WIDGET_ERROR_FAULT;
	}

	evas_object_resize(wid->win, w, h);

	/* Conformant */
	wid->conform = elm_conformant_add(wid->win);
	evas_object_size_hint_weight_set(wid->conform, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_win_resize_object_add(wid->win, wid->conform);
	evas_object_show(wid->conform);

	/* Label*/
//

	/* First label (for the sensor support) */
	wid->label0 = elm_label_add(wid->conform);
	evas_object_resize(wid->label0, w, h / 5);
	evas_object_move(wid->label0, w / 10, h / 5);
	evas_object_show(wid->label0);
	elm_object_text_set(wid->label0, "Msg -");

	/* Second label (for the current acceleration value) */
	wid->label1 = elm_label_add(wid->conform);
	evas_object_resize(wid->label1, w, h / 5);
	evas_object_move(wid->label1, w / 10, 2* h / 5);
	evas_object_show(wid->label1);
	elm_object_text_set(wid->label1, "Val -");

	/* Third label (for the maximum value) */
	wid->label2 = elm_label_add(wid->conform);
	evas_object_resize(wid->label2, w, h / 5);
	evas_object_move(wid->label2, w / 10, 3* h / 5);
	evas_object_show(wid->label2);
	elm_object_text_set(wid->label2, "Max ");



	/* Show window after base gui is set up */
	evas_object_show(wid->win);

	/* Check the sensor support */
	show_is_supported(wid);
	start_accelerator_sensor(wid);

	widget_app_context_set_tag(context, wid);
	return WIDGET_ERROR_NONE;
}

static int
widget_instance_destroy(widget_context_h context, widget_app_destroy_type_e reason, bundle *content, void *user_data)
{
	widget_instance_data_s *wid = NULL;
	widget_app_context_get_tag(context,(void**)&wid);

	if (wid->win)
		evas_object_del(wid->win);

	free(wid);

	return WIDGET_ERROR_NONE;
}

static int
widget_instance_pause(widget_context_h context, void *user_data)
{
	/* Take necessary actions when widget instance becomes invisible. */
	return WIDGET_ERROR_NONE;

}

static int
widget_instance_resume(widget_context_h context, void *user_data)
{
	/* Take necessary actions when widget instance becomes visible. */
	return WIDGET_ERROR_NONE;
}

static int
widget_instance_update(widget_context_h context, bundle *content,
                             int force, void *user_data)
{
	/* Take necessary actions when widget instance should be updated. */
	return WIDGET_ERROR_NONE;
}

static int
widget_instance_resize(widget_context_h context, int w, int h, void *user_data)
{
	/* Take necessary actions when the size of widget instance was changed. */
	return WIDGET_ERROR_NONE;
}

static void
widget_app_lang_changed(app_event_info_h event_info, void *user_data)
{
	/* APP_EVENT_LANGUAGE_CHANGED */
	char *locale = NULL;
	app_event_get_language(event_info, &locale);
	elm_language_set(locale);
	free(locale);
}

static void
widget_app_region_changed(app_event_info_h event_info, void *user_data)
{
	/* APP_EVENT_REGION_FORMAT_CHANGED */
}

static widget_class_h
widget_app_create(void *user_data)
{
	/* Hook to take necessary actions before main event loop starts.
	   Initialize UI resources.
	   Make a class for widget instance.
	*/
	app_event_handler_h handlers[5] = {NULL, };

	widget_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED],
		APP_EVENT_LANGUAGE_CHANGED, widget_app_lang_changed, user_data);
	widget_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED],
		APP_EVENT_REGION_FORMAT_CHANGED, widget_app_region_changed, user_data);

	widget_instance_lifecycle_callback_s ops = {
		.create = widget_instance_create,
		.destroy = widget_instance_destroy,
		.pause = widget_instance_pause,
		.resume = widget_instance_resume,
		.update = widget_instance_update,
		.resize = widget_instance_resize,
	};

	return widget_app_class_create(ops, user_data);
}

static void
widget_app_terminate(void *user_data)
{
	/* Release all resources. */
}

int
main(int argc, char *argv[])
{
	widget_app_lifecycle_callback_s ops = {0,};
	int ret;

	ops.create = widget_app_create;
	ops.terminate = widget_app_terminate;

	ret = widget_app_main(argc, argv, &ops, NULL);
	if (ret != WIDGET_ERROR_NONE) {
		dlog_print(DLOG_ERROR, LOG_TAG, "widget_app_main() is failed. err = %d", ret);
	}

	return ret;
}


