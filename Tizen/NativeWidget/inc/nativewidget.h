#ifndef __nativewidget_H__
#define __nativewidget_H__

#include <widget_app.h>
#include <widget_app_efl.h>
#include <Elementary.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "nativewidget"

#if !defined(PACKAGE)
#define PACKAGE "org.example.nativewidget"
#endif

#endif /* __nativewidget_H__ */
